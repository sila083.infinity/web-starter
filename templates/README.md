---
toc: content
---

<https://umijs.org/docs/guides/generator#%E5%AF%B9%E9%A1%B5%E9%9D%A2%E6%A8%A1%E6%9D%BF%E5%86%85%E5%AE%B9%E8%BF%9B%E8%A1%8C%E8%87%AA%E5%AE%9A%E4%B9%89>

# micro generator

There are many built-in micro-generators in Umi to help you quickly complete some tedious work in development.

## how to use

The following command will list all currently available generators, and you can choose the function you use interactively, with detailed prompts.

```
$ umi generate
# 或者
$ umi g
```

You can also use the corresponding generator `umi g <generatorName>`in the form of .

## generator list

### page builder

To quickly generate a new page, there are the following ways to use it.

#### basic use

Interactively enter the page name and file generation method:

```
$umi g page
? What is the name of page? › mypage
? How dou you want page files to be created? › - Use arrow-keys. Return to submit.
❯   mypage/index.{tsx,less}
    mypage.{tsx,less}
```

Generate directly:

```
$umi g page foo
Write: src/pages/foo.tsx
Write: src/pages/foo.less
```

The page is generated as a directory, and the components and style files of the page are under the directory:

```
$umi g page bar --dir
Write: src/pages/bar/index.less
Write: src/pages/bar/index.tsx
```

Nested generated pages:

```
$umi g page far/far/away/kingdom
Write: src/pages/far/far/away/kingdom.tsx
Write: src/pages/far/far/away/kingdom.less
```

Generate multiple pages in batches:

```
$umi g page  page1  page2   a/nested/page3
Write: src/pages/page1.tsx
Write: src/pages/page1.less
Write: src/pages/page2.tsx
Write: src/pages/page2.less
Write: src/pages/a/nested/page3.tsx
Write: src/pages/a/nested/page3.less
```

#### Customize page template content

If the default templates used by the page builder don't meet your needs, you can customize the template content.

Execute the `--eject`command :

After executing the command, the page builder will write its raw templates to the project's `/templates/page`directory :

```
.
├── package.json
└── templates
    └── page
        ├── index.less.tpl
        └── index.tsx.tpl
```

##### Using template variables

Both template files support template syntax, you can insert variables like this:

```
import React from 'react';
import './{{{name}}}.less'
 
const message = '{{{msg}}}'
const count = {{{count}}}
```

Parameter values can be customized:

```
$umi g page foo --msg "Hello World" --count 10
```

After running the command, the content of the generated page is as follows:

```
import React from 'react';
import './foo.less'
 
const message = 'Hello World'
const count = 10
```

If you don't need to use template variables, you can omit the `.tpl`suffix name and `index.tsx.tpl`abbreviate as `index.tsx`, `index.less.tpl`abbreviate as `index.less`.

##### default variable

In the content generated in the previous section, we didn't specify `name`it, but it was still set. This is because it belongs to the preset variables in the template. The following are all the preset variables of the current page template:

| parameter | Defaults | illustrate |
| --- | --- | --- |
| `name` | \- | The name of the current file. If executed `pnpm umi g page foo`, two files `pages/foo.tsx`and `pages/foo.less`, where `name`the value of is "foo". |
| `color` | \- | Randomly generates an RGB color. |
| `cssExt` | `less` | The suffix of the style file. |

For more on template syntax, check out [mustache](https://www.npmjs.com/package/mustache) .

##### `dir`model

If you do not use the `dir`mode , if your page template folder has only customized a template file, the missing file will automatically select the default template file.

If you use the `dir`mode , its generated content will be consistent with your page custom template folder, and the default template will be used only when the page custom template folder is empty. If your page custom template folder content is as follows:

```
.
├── a.tsx
└── index.tsx.tpl
```

The resulting directory will be:

```
.
├── a.tsx
└── index.tsx
```

##### go back

If you still want to continue to use the default template, you can specify `--fallback`that the user-defined template is no longer used at this time:

```
$umi g page foo --fallback
```

### component generator

Generate the components required by the project under the `src/components/`directory . Like the Page Builder, the Component Builder can be generated in a variety of ways.

#### basic use

Generate interactively:

```
$umi g component
✔ Please input you component Name … foo
Write: src/components/Foo/index.ts
Write: src/components/Foo/Foo.tsx
```

Generate directly:

```
$umi g component bar
Write: src/components/Bar/index.ts
Write: src/components/Bar/Bar.tsx
```

Nested generation:

```
$umi g component group/subgroup/baz
Write: src/components/group/subgroup/Baz/index.ts
Write: src/components/group/subgroup/Baz/Baz.tsx
```

Batch generation:

```
$umi g component apple banana orange
Write: src/components/Apple/index.ts
Write: src/components/Apple/Apple.tsx
Write: src/components/Banana/index.ts
Write: src/components/Banana/Banana.tsx
Write: src/components/Orange/index.ts
Write: src/components/Orange/Orange.tsx
```

#### Customize component template content

Like [Page Builder](https://umijs.org/docs/guides/generator#%E5%AF%B9%E9%A1%B5%E9%9D%A2%E6%A8%A1%E6%9D%BF%E5%86%85%E5%AE%B9%E8%BF%9B%E8%A1%8C%E8%87%AA%E5%AE%9A%E4%B9%89) , Component Builder also supports customization of template content. First, write the original template to the project's `/templates/component`directory :

##### Using template variables

```
$umi g component foo --msg "Hello World"
```

Custom component templates can omit the `.tpl`suffix . You `index.ts.tpl`can abbreviate `index.ts`as .`component.tsx.tpl``component.tsx`

##### default variable

| parameter | Defaults | illustrate |
| --- | --- | --- |
| `compName` | \- | The name of the current component. If implemented `pnpm umi g component foo`, `compName`the value is `Foo`. |

##### go back

```
$umi g component foo --fallback
```

### Route API generator

Generate a template file for the routeAPI function.

Generate interactively:

```
$umi g api
✔ please input your api name: … starwar/people
Write: api/starwar/people.ts
```

Generate directly:

```
$umi g api films
Write: api/films.ts
```

Nested generators:

```
$umi g api planets/[id]
Write: api/planets/[id].ts
```

Batch generation:

```
$umi g api spaceships vehicles species
Write: api/spaceships.ts
Write: api/vehicles.ts
Write: api/species.ts
```

### Mock generator

Generate the template file of the [Mock function, and refer to the](https://umijs.org/docs/guides/mock) [documentation](https://umijs.org/docs/guides/mock) for the specific implementation of the mock .

Generate interactively:

```
$umi g mock
✔ please input your mock file name … auth
Write: mock/auth.ts
```

Generate directly:

```
$umi g mock acl
Write: mock/acl.ts
```

Nested generation:

```
$umi g mock users/profile
Write: mock/users/profile.ts
```

### Prettier configuration generator

Generate [prettier](https://prettier.io/) configuration for the project. After the command is executed, `umi`the recommended prettier configuration will be generated and the corresponding dependencies will be installed.

```
$umi g prettier
info  - Write package.json
info  - Write .prettierrc
info  - Write .prettierignore
```

### Jest config generator

Generate the [jest](https://jestjs.io/) configuration for the project. After the command is executed, `umi`the Jest configuration will be generated and the corresponding dependencies will be installed. Choose whether to use [@testing-library/react](https://www.npmjs.com/package/@testing-library/react) for UI testing according to your needs.

```
$umi g jest
✔ Will you use @testing-library/react for UI testing?! … yes
info  - Write package.json
info  - Write jest.config.ts
```

### Tailwind CSS config generator

Enable [Tailwind CSS](https://tailwindcss.com/) configuration for the project. After the command is executed, `umi`Tailwind CSS will be generated and the corresponding dependencies will be installed.

```
$umi g tailwindcss
info  - Write package.json
set config:tailwindcss on /Users/umi/playground/.umirc.ts
set config:plugins on /Users/umi/playground/.umirc.ts
info  - Update .umirc.ts
info  - Write tailwind.config.js
info  - Write tailwind.css
```

### DvaJS configuration generator

Enable [Dva](https://dvajs.com/) configuration for the project, after the command is executed, `umi`Dva will be generated

```
$umi g dva
set config:dva on /Users/umi/umi-playground/.umirc.ts
set config:plugins on /Users/umi/umi-playground/.umirc.ts
info  - Update config file
info  - Write example model
```

### Precommit configuration generator

Generate [precommit](https://typicode.github.io/husky) configuration for the project. After the command is executed, `umi`the husky and Git commit message format verification behavior will be added for us, and the code in the Git temporary storage area will be formatted by default before each Git commit.

> Note: If it is an initialized `@umijs/max`project , the generator is usually not needed, because husky has already been configured

```
$umi g precommit
info  - Update package.json for devDependencies
info  - Update package.json for scripts
info  - Write .lintstagedrc
info  - Create .husky
info  - Write commit-msg
info  - Write pre-commit
```
