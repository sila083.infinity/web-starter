import { defineConfig } from 'umi'

console.log('config.dev is working')

// all UMI Dev config here
export default defineConfig({
  externals: {
    react: 'window.React',
    'react-dom': 'window.ReactDOM',
  },
  scripts: [
    '//unpkg.com/react@18.2.0/umd/react.development.js',
    '//unpkg.com/react-dom@18.2.0/umd/react-dom.development.js',
  ],

})
