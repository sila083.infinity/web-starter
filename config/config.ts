import { defineConfig } from 'umi';
// import d from '@umijs/plugins/dist/initial-state' // all plugins here
console.log('config is working');

export default defineConfig({
  mfsu: {}, // mfsu is webpage plug in for improve speed faster than vite (enabled by default)
  svgr: {},
  define: {},
  // proxy:{},
  mock: {
    // include: ['pages/**/_mock.ts'],
  },
  // https:{}, // local dev with https
  // ssr: {} // enabled server side
  // mfsu: {} // mfsu is webpage plug in for improve speed faster than vite (enabled by default)
  npmClient: 'yarn', // node package manager min node 16
  deadCode: {
    // exclude: ['pages/unused/**'],
  }, // Detect unused files and export, only enabled during build phase.
  analyze: {}, //analyze on build bundle size https://umijs.org/docs/api/config#analyze
  /**
   * Umi plugin
   * @see https://github.com/umijs/plugins/tree/master/packages
   */
  plugins: [
    '@umijs/plugins/dist/tailwindcss', // enabled tailwindcss
    '@umijs/plugins/dist/moment2dayjs', // replace moment2dayjs for antd
    '@umijs/plugins/dist/antd', // enabled antd5
    '@umijs/plugins/dist/valtio', // enabled valtio global state
    '@umijs/plugins/dist/request', // enable request Axios and useRequest
    '@umijs/plugins/dist/locale',
    '@umijs/plugins/dist/model',
    '@umijs/plugins/dist/initial-state',
    // "@umijs/plugins/dist/qiankun", // micro front-end
  ],
  // ===================== Plugin configs ======================
  request: {
    dataField: '' as '', // return date field to match with api
  }, // config axios & ahooks useRequest
  valtio: {}, // state management
  model: {}, // state management
  antd: {}, // antd design v5
  moment2dayjs: {
    preset: 'antd',
    plugins: [],
  },
  // initialState:{},
  autoprefixer: {},
  tailwindcss: {}, // enabled tailwindcss
  routePrefetch: {},
  manifest: {},
  clientLoader: {},
  // lowImport:{} // auto import
  // Configure additional link tags.
  // links: [],
  // styles: [],
  // ===================== End Plugin configs ======================
  // 国际化配置 https://umijs.org/zh-CN/plugins/plugin-locale
  locale: {
    default: 'en-US',
    antd: true,
    baseNavigator: true,
  },
  // Add the attribute crossorigin="anonymous" to all non-third-party scripts, usually used to count script errors.
  crossorigin: false,
  //Enable TypeScript compile-time type checking
  // forkTSChecker: {},
  //whether the hash configuration allows the generated file to contain a hash suffix, which is usually used for incremental publishing and avoiding browser loading cache
  // hash: true,
  // clientLoader: {}, // https://umijs.org/docs/api/config#clientloader
  // Do not recognize files in the components and models directories as routes
  conventionRoutes: {
    exclude: [/\/components\//, /\/models\//],
  },
  // alias configuration
  alias: {},
  chainWebpack(config, { webpack }) {
    config.module.rule('ts-in-node_modules').include.clear();
    return config;
  },
});
