module.exports = {
  extends: ['plugin:valtio/recommended'],

  rules: {
    'react/jsx-no-undef': [0, { allowGlobals: true }],
  },
}
