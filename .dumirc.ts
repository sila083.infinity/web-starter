import { defineConfig } from 'dumi';

export default defineConfig({
  plugins: [
    // require.resolve('@umijs/plugins/dist/antd'), // enabled antd5
    require.resolve('@umijs/plugins/dist/tailwindcss'),
  ],
  mfsu: false,
  tailwindcss: {},
  // antd:{},
  locales: [{ id: 'en-US', name: 'EN', suffix: '' }],
  // apiParser: {},
  resolve: {
    // Configure the entry file path, API parsing will start from here
    // entryFile: './src/components/index.ts',
    // auto generate docs
    atomDirs: [
      // utils
      { type: 'components', dir: 'src/components' },
      { type: 'readme', dir: '.' },
      { type: 'src', dir: 'src' },
    ],
  },
  favicons: [
    'https://gw.alipayobjects.com/zos/bmw-prod/d3e3eb39-1cd7-4aa5-827c-877deced6b7e/lalxt4g3_w256_h256.png',
  ],
  alias:{},
  exportStatic:{},
  // autoAlias: false,
  outputPath: 'dist-docs',
  // publicPath: '/web-docs/',
  // base: '/web-docs/',
  themeConfig: {
    name: 'Web Docs',
    logo: 'https://gw.alipayobjects.com/zos/bmw-prod/d3e3eb39-1cd7-4aa5-827c-877deced6b7e/lalxt4g3_w256_h256.png',
    footer: `Open-source MIT Licensed | Copyright © 2019-${new Date().getFullYear()}<br /> Powered by Web`,
  },
  chainWebpack(config, { webpack }) {
    config.module.rule('ts-in-node_modules').include.clear();
    return config;
  },
});
