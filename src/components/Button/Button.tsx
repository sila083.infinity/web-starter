import React from 'react';

type Button = {
  title?: string;
  data?: {
    name: string;
    age: number;
  };
};
export const Button = ({ title }: Button) => {
  return <button>{title}</button>;
};
