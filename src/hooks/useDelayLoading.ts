import asyncSleep from '@/utils/async';
import { useAsyncEffect } from 'ahooks';
import { useState } from 'react';

/**
 * useDelayLoading: will delay loading until finished delay time is reached
 * @param delay
 * @default delay in 300 milliseconds
 */
export function useDelayLoading(delay = 300) {
  const [loading, setLoading] = useState(true);

  useAsyncEffect(async () => {
    setLoading(await asyncSleep(delay, false));
  }, [delay]);

  return {
    loading,
    setLoading,
  };
}
