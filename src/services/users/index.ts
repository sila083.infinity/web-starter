import { request } from 'umi';

export const getUserData = async () => {
  return await request<ApiUser.userInfoData>('/users');
};
