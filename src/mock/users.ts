import { defineMock } from "umi";

export default defineMock({
  '/api/users': {
    success: true,
    data: ['sorrycc', 'chencheng'],
  },
})
