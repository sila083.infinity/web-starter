/**
 * 
 * @param ms 
 * @param value 
 */
export default function asyncSleep<T>(ms = 0, value?: T): Promise<T> {
    return new Promise((resolve) => setTimeout(() => resolve(value as T), ms));
}
