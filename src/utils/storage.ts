import { proxy, subscribe, snapshot } from "umi";



/**
 * @example 
 *  const state = proxyWithPersistant({
        count: 0,
        }, 
        {
            key: 'count',
        }); 
 */
export function proxyWithPersist<V>(val: V, opts: {
    key: string;
}) {
    const local = localStorage.getItem(opts.key);
    const state = proxy(local ? JSON.parse(local) : val);
    subscribe(state, () => {
        localStorage.setItem(opts.key, JSON.stringify(snapshot(state)));
    });
    return state;
}

