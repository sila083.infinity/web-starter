import { Link, Outlet, SelectLang } from 'umi';
import { useLayout } from './useLayout';

export default function Layout() {
  const {} = useLayout();

  return (
    <div className="p-9">
      <ul className="flex gap-x-2  border-b border-gray-300 mb-8">
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/users">/Normal route</Link>
        </li>
        <li>
          <Link to="/users/foo">Dynamic route</Link>
        </li>
        <li>
          <Link to="/dev">Dev Docs</Link>
        </li>
        <SelectLang reload={false} />
      </ul>
      <Outlet />
    </div>
  );
}
