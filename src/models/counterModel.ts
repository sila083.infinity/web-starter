import { getUserData } from '@/services/users';
import { useCookieState, useCreation, useMemoizedFn } from 'ahooks';
import { useState, useCallback } from 'react';
import { useRequest } from 'umi';

export default function counterModel() {
  const [counter, setCounter] = useState(0);
  const [counterLs, setCounterLs] = useCookieState('useCounter', {
    defaultValue: '0',
  });

  const {
    data: userData,
    run: runUser,
    refresh: refreshUser,
    loading: loadingUser,
  } = useRequest(getUserData, {
    manual: true,
  });

  const increment = useCallback(() => setCounter((c) => c + 1), []);
  const decrement = useCallback(() => setCounter((c) => c - 1), []);
  const decrementLs = useCallback(
    () => setCounterLs((v) => String(Number(v) - 1)),
    [],
  );
  const incrementLs = useCallback(
    () => setCounterLs((v) => String(Number(v) + 1)),
    [],
  );

  return {
    loadingUser,
    userData,
    counterLs,
    counter,
    refreshUser,
    increment,
    decrement,
    incrementLs,
    decrementLs,
    runUser,
  };
}
