import { proxyWithComputed } from 'umi';

export const _appStore = proxyWithComputed(
  {
    count: 0,
    inc: () => {
      ++_appStore.count
    },
    dec: () => {
      --_appStore.count
    },
    async addAsync() {
      _appStore.count = 10
    },
  },
  {
    doubleCount: (state) => state.count * 2,
  },
)
