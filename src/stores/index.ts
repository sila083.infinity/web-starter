import { proxy, proxyWithDevtools } from "@umijs/valtio";
import { useSnapshot } from "umi"
import { _appStore } from "./app"

export const useAppStore = () => useSnapshot(_appStore)

// combine all store 
export const allStore = proxy({ _appStore })

// redux devtool
proxyWithDevtools(allStore);