import { useDelayLoading } from '@/hooks';
import { Skeleton } from 'antd';

/**
 * @description this page will not showing in production
 */
const DevDocs = () => {
  const { loading } = useDelayLoading(3000);

  if (process.env.NODE_EN === 'production') return null;

  return (
    <>
      <Skeleton loading={loading} active />
      <iframe
        className="w-full h-[calc(100vh-200px)] bg-white  rounded-lg lg:transition-all "
        src={'https://web-starter-docs.netlify.app/readmes'}
        style={{ maxWidth: '100%', visibility: loading ? 'hidden' : 'visible' }}
      />
    </>
  );
};

export default DevDocs;
