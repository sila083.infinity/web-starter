import { useAppStore } from "@/stores/index";

export default function useUserLogic() {
  const appStore = useAppStore();


  return {
    appStore,
  };
}
