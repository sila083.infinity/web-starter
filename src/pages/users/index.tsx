import Button from '@/components/Button';

export default function HomePage() {
  return (
    <div className="container px-8">
      <Button title='test component' />
    </div>
  );
}
