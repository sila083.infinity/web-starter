import { useAppStore } from '@/stores';
import { Avatar, Button, Card, List, Space, Typography } from 'antd';
import { useEffect } from 'react';
import { useIntl, FormattedMessage, useModel } from 'umi';

export default function HomePage() {
  const { formatMessage } = useIntl();
  const {
    counter,
    decrement,
    increment,
    counterLs,
    decrementLs,
    incrementLs,
    runUser,
    userData,
    refreshUser,
    loadingUser,
  } = useModel('counterModel');
  const { doubleCount, count, dec, inc } = useAppStore();

  console.log('runUser', userData);

  const data = userData?.data
    ?.map((i) => {
      return { title: i.name, ...i };
    })
    .slice(0, 4);

  useEffect(() => {
    runUser();
  }, []);

  return (
    <div className="container px-8 flex flex-wrap gap-4">
      <Card title="Antd UI">
        <Space>
          <Button type="primary"> button </Button>
        </Space>
      </Card>

      <Card size="small" title="I18n, Tailwind">
        <Space direction="vertical">
          <Space>
            I18n hooks
            <Typography.Title level={3}>
              {formatMessage({ id: 'menu.login', defaultMessage: 'login' })}
            </Typography.Title>
          </Space>
          <Space>
            <Typography.Text>i18n Component:</Typography.Text>
            <FormattedMessage id="documents" />
          </Space>
          <h1 className="font-extrabold text-transparent text-3xl bg-clip-text bg-gradient-to-r from-purple-400 to-green-600">
            Tailwind Css
          </h1>
        </Space>
      </Card>

      <Card title="State">
        <Space>
          <Card title="model state" className="mb-2">
            <Space size="small">
              <Button danger shape="circle" onClick={decrement}>
                -
              </Button>
              <Button type="text">{counter}</Button>
              <Button shape="circle" type="primary" onClick={increment}>
                +
              </Button>
            </Space>
          </Card>
          <Card title="Model state persist">
            <Space size="small">
              <Button danger shape="circle" onClick={decrementLs}>
                -
              </Button>
              <Button type="text">{counterLs}</Button>
              <Button shape="circle" type="primary" onClick={incrementLs}>
                +
              </Button>
            </Space>
          </Card>
          <Card
            title="Valito state"
            extra={
              <Typography.Text className="ml-2">
                Double Count: {doubleCount}
              </Typography.Text>
            }
          >
            <Space size="small">
              <Button danger shape="circle" onClick={dec}>
                -
              </Button>
              <Button type="text">{count}</Button>
              <Button shape="circle" type="primary" onClick={inc}>
                +
              </Button>
            </Space>
          </Card>
        </Space>
        <Card
          loading={loadingUser}
          title="Request"
          extra={<Button onClick={refreshUser}>Refetch</Button>}
        >
          <List
            itemLayout="horizontal"
            dataSource={data}
            renderItem={(item) => (
              <List.Item>
                <List.Item.Meta
                  title={<a href="https://ant.design">{item.title}</a>}
                  description={item?.email}
                />
              </List.Item>
            )}
          />
        </Card>
      </Card>
    </div>
  );
}
