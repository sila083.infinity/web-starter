import menu from './menu'
import common from './common'
import { getMenuKey } from '@/utils/obj'

export const allTranslation = (langKey: LangKey) => {
  return {
    ...getMenuKey(langKey, common),
    ...getMenuKey(langKey, menu),
  }
}
