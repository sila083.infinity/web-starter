// Manual export global type

export { }

declare global {
  // custom Window type 
  interface Window {
    routerBase: string
  }
  /**
   *  ========= Locale ================
   */
  type LangKey = 'en-US' | 'zh-CN'
}
